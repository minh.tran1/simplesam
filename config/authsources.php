<?php
require_once JPATH_BASE . '/administrator/components/com_sfs/helpers/sso.php';
$config = JFactory::getConfig();
$airline = SAirline::getAirlineParamsByIataCode($config->get('airline_code'));
$airlineParams = json_decode($airline->params);
$oauthConfig = (array) $airlineParams->oauth2_config;
// add authenticate source into oauth config array
array_unshift($oauthConfig, $config->get('oauth_module'));

$config = [
    'SFS360' => [
        'saml:SP',
        'NameIDPolicy' => false,
        'RelayState' => \SimpleSAML\Module::getBaseSfsURL() . '/index.php?option=com_sfsuser&task=sso.login',
        'privatekey' => 'saml.pem',
        'certificate' => 'saml.crt',
        'entityID' => $airlineParams->sp_config->entity_id,
        'idp' => $airlineParams->saml_config->entity_id
    ],

    strtoupper($config->get('airline_code')) => $oauthConfig,

    // This is a authentication source which handles admin authentication.
    'admin' => [
        // The default is to use core:AdminPassword, but it can be replaced with
        // any authentication source.

        'core:AdminPassword',
    ],
];
