<?php

if (!array_key_exists('state', $_REQUEST)) {
    throw new \Exception('Lost OAuth Client State');
}
$state = \SimpleSAML\Auth\State::loadState(
    $_REQUEST['state'],
    \SimpleSAML\Module\lufthansa\Auth\Source\Lufthansa::STAGE_INIT
);

$state['lufthansa:code'] = $_REQUEST['code'];

// find authentication source
assert(array_key_exists(\SimpleSAML\Module\lufthansa\Auth\Source\Lufthansa::AUTHID, $state));
$sourceId = $state[\SimpleSAML\Module\lufthansa\Auth\Source\Lufthansa::AUTHID];

$source = \SimpleSAML\Auth\Source::getById($sourceId);
if ($source === null) {
    throw new \Exception('Could not find authentication source with id '.$sourceId);
}

$source->finalStep($state);

\SimpleSAML\Auth\Source::completeAuth($state);
