<?php

namespace SimpleSAML\Module\lufthansa\Auth\Source;

use SimpleSAML\Module;

/**
 * Authenticate using Lufthansa Platform.
 *
 * @package SimpleSAMLphp
 */
class Lufthansa extends \SimpleSAML\Auth\Source
{

    private $clientId;
    private $clientSecret;
    private $authorizeUrl;
    private $tokenUrl;

    /**
     * The string used to identify our states.
     */
    const STAGE_INIT = 'lufthansa:init';

    /**
     * The key of the AuthId field in the state.
     */
    const AUTHID = 'lufthansa:AuthId';

    /**
     * Constructor for this authentication source.
     *
     * @param array $info Information about this authentication source.
     * @param array $config Configuration.
     * @throws \Exception
     */
    public function __construct($info, $config)
    {
        assert(is_array($info));
        assert(is_array($config));

        // Call the parent constructor first, as required by the interface
        parent::__construct($info, $config);

        $arrConfig = ['client_id', 'client_secret', 'authorize_url', 'token_url'];
        foreach ($arrConfig as $item) {
            if (!array_key_exists($item, $config)) {
                throw new \Exception('Lufthansa authentication source is not properly configured: missing [' . $item . ']');
            }
        }
        $this->clientId = $config['client_id'];
        $this->clientSecret = $config['client_secret'];
        $this->authorizeUrl = $config['authorize_url'];
        $this->tokenUrl = $config['token_url'];
    }

    /**
     * Log-in using Lufthansa platform
     *
     * @param array &$state Information about the current authentication.
     */
    public function authenticate(&$state)
    {
        assert(is_array($state));

        // we are going to need the authId in order to retrieve this authentication source later
        $state[self::AUTHID] = $this->authId;

        $stateID = \SimpleSAML\Auth\State::saveState($state, self::STAGE_INIT);
        \SimpleSAML\Logger::debug('Lufthansa auth state id = ' . $stateID);

        $random = bin2hex(openssl_random_pseudo_bytes(32));
        $verifier = $this->base64url_encode(pack('H*', $random));
        setcookie('verifier', $verifier, time() + 60 * 30);
        $code_challenge = $this->base64url_encode(pack('H*', hash('sha256', $verifier)));
        setcookie('code_challenge', $code_challenge, time() + 60 * 30);

        $authorizeURL = $this->authorizeUrl .
            '?response_type=code' .
            '&client_id=' . $this->clientId .
            '&scope=openid' .
            '&code_challenge_method=S256' .
            '&redirect_uri=' . urlencode(\SimpleSAML\Module::getModuleURL('lufthansa') . '/linkback.php') .
            '&code_challenge=' . $code_challenge .
            '&state=' . urlencode($stateID);

        \SimpleSAML\Utils\HTTP::redirectTrustedURL($authorizeURL);
    }

    public function finalStep(&$state)
    {
        \SimpleSAML\Logger::debug(
            "Lufthansa oauth: Using this verification code [" . $state['lufthansa:code'] . "]"
        );

        $postData = 'client_id=' . $this->clientId .
            '&grant_type=authorization_code' .
            '&redirect_uri=' . \SimpleSAML\Module::getModuleURL('lufthansa') . '/linkback.php' .
            '&code=' . urlencode($state['lufthansa:code']) .
            '&code_verifier=' . $_COOKIE['verifier'];

        $context = [
            'http' => [
                'method' => 'POST',
                'header' => "Content-type: application/x-www-form-urlencoded\r\nAuthorization: Basic " . base64_encode($this->clientId . ':' . $this->clientSecret) . "\r\n",
                'content' => $postData
            ],
        ];

        $result = \SimpleSAML\Utils\HTTP::fetch($this->tokenUrl, $context);

        $response = json_decode($result, true);

        $attributes = [];
        $attributes['code'] = $state['lufthansa:code'];
        foreach ($response as $key => $value) {
            if (is_string($value)) {
                $attributes[$key] = [(string)$value];
            }
        }

        $state['Attributes'] = $attributes;
    }

    private function base64url_encode($plainText)
    {
        $base64 = base64_encode($plainText);
        $base64 = trim($base64, "=");
        $base64url = strtr($base64, '+/', '-_');

        return ($base64url);
    }
}
